import { Component } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Pelicula } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from '../components/detalle/detalle.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  busqueda = '';
  peliculasResultado: Pelicula[] = [];
  sinResultados = false;
  sugerencias = true;

  constructor( private moviesService: MoviesService, private modalCtrl: ModalController ) {}

  buscarPelicula( event ) {

    if (event.detail.value === '') {
      this.sugerencias = true;
    }else{
      this.sugerencias = false;
      const pelicula = event.detail.value;
      this.busqueda = event.detail.value;
      this.moviesService.getBusqueda(pelicula).subscribe( resp => {
        if (resp['total_results'] === 0){
          this.sinResultados = true;
        }else{
          this.sinResultados = false;
        }
        this.peliculasResultado = resp['results'];
        console.log('busqueda', resp);
      });
    }
  }

  async mostrarDetalle( id: string ) {
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: {
        id
      }
    });

    modal.present();
  }

}
