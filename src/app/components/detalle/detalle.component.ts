import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { ModalController } from '@ionic/angular';
import { DetallePelicula } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {

  @Input() id;

  pelicula: DetallePelicula = {};

  constructor( private moviesService: MoviesService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.moviesService.getDetalle( this.id ).subscribe( resp => {
      console.log('detalle', resp);
      this.pelicula = resp;
    });
  }

  volver(){
    this.modalCtrl.dismiss();
  }

}
