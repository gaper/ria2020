import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RespuestaTMDB, DetallePelicula } from '../interfaces/interfaces';

const URL = environment.url;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private pagePopulares = 0;

  constructor( private http: HttpClient ) { }

  private getData<T>( query: string) {
    query = URL + query;
    query += `&api_key=${ apiKey }&language=es&include_image_language=es`;

    return this.http.get<T>( query );
  }

  getPopulares() {
    this.pagePopulares ++;

    const query = `/discover/movie?sort_by=popularity.desc&page=${this.pagePopulares}`;

    return this.getData<RespuestaTMDB>(query);
  }

  getDetalle(id: string) {
    const query = `/movie/${id}?a=0`;

    return this.getData<DetallePelicula>(query);
  }

  getBusqueda(pelicula: string){
    const query = `/search/movie?query=${pelicula}`;

    return this.getData(query);
  }

}
